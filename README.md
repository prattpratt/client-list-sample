## Client List Sample App

1. Create an app for managing client list. The list can be long.
2. Use Kotlin
3. Follow [material design](https://material.io/design/guidelines-overview) guidelines
4. No persistence layer needed.

#### This is the empty main screen:

![Screen 1](art/Screenshot_2022-02-18-12-28-58-808_com.example.clients.android.jpg)

#### This screen should prompt user to enter body weight in LB or kg. Please choose UI:

![Screen 2](art/Screenshot_2022-02-18-12-27-18-845_com.example.clients.android.jpg)

Choose appropriate steps indicator.

#### Please suggest appropriate UI to enter Date Of Birth:

![Screen 3](art/Screenshot_2022-02-18-12-27-23-548_com.example.clients.android.jpg)

#### Organize UI for taking or choosing a photo:

![Screen 4](art/Screenshot_2022-02-18-12-27-27-893_com.example.clients.android.jpg)

#### This is the filled main screen:

![Screen 5](art/Screenshot_2022-02-18-12-27-06-012_com.example.clients.android.jpg)
