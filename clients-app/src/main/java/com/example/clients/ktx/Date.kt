package com.example.clients.ktx

import java.text.DateFormat
import java.util.*

fun Date.toString(
    dateStyle: DateStyle
): String {
    return DateFormat.getDateInstance(dateStyle.value).format(this)
}

fun Date.toString(
    timeStyle: TimeStyle
): String {
    return DateFormat.getTimeInstance(timeStyle.value).format(this)
}

fun Date.toString(
    dateStyle: DateStyle,
    timeStyle: TimeStyle
): String {
    return DateFormat.getDateTimeInstance(dateStyle.value, timeStyle.value).format(this)
}
