package com.example.clients.ktx

import java.text.DateFormat

enum class TimeStyle(val value: Int) {
    /**
     * US locale examples:
     *
     * 8:15:28 AM Moscow Standard Time
     *
     * 11:35:52 PM Moscow Standard Time
     */
    FULL(DateFormat.FULL),

    /**
     * US locale examples:
     *
     * 8:15:28 AM GMT+03:00
     *
     * 11:35:52 PM GMT+03:00
     */
    LONG(DateFormat.LONG),

    /**
     * US locale examples:
     *
     * 08:15:28
     *
     * 23:35:52
     */
    MEDIUM(DateFormat.MEDIUM),

    /**
     * US locale examples:
     *
     * 08:15
     *
     * 23:35
     */
    SHORT(DateFormat.SHORT),
}
