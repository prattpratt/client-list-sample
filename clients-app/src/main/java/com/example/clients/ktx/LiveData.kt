package com.example.clients.ktx

import androidx.lifecycle.MutableLiveData

fun <T> MutableLiveData<List<T>>.remove(item: T?) {
    value = value?.toMutableList()?.apply { remove(item) }
}
