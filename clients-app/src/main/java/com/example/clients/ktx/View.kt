package com.example.clients.ktx

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

val View.inflater: LayoutInflater get() = LayoutInflater.from(context)

fun ViewGroup.inflate(@LayoutRes layoutRes: Int): View = inflater.inflate(layoutRes, this, false)
