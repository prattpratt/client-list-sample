package com.example.clients.ktx

import java.text.DateFormat

enum class DateStyle(val value: Int) {
    /**
     * US locale examples:
     *
     * Thursday, September 9, 2021
     *
     * Thursday, September 16, 2021
     * */
    FULL(DateFormat.FULL),

    /**
     * US locale examples:
     *
     * September 9, 2021
     *
     * September 16, 2021
     */
    LONG(DateFormat.LONG),

    /**
     * US locale examples:
     *
     * Sep 9, 2021
     *
     * Sep 16, 2021
     */
    MEDIUM(DateFormat.MEDIUM),

    /**
     * US locale examples:
     *
     * 9/9/21
     *
     * 9/16/21
     */
    SHORT(DateFormat.SHORT),
}
