package com.example.clients.data

import android.net.Uri
import com.example.clients.data.model.Client
import java.util.*

interface ClientRepository {

    suspend fun addClient(weight: Int, dateOfBirth: Date, photo: Uri)

    suspend fun updateClient(id: UUID, weight: Int, dateOfBirth: Date, photo: Uri)

    suspend fun removeClient(id: UUID): Client?

    suspend fun getClient(id: UUID): Client?

    suspend fun getClients(): List<Client>
}
