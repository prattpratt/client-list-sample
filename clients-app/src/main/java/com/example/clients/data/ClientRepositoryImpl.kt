package com.example.clients.data

import android.net.Uri
import com.example.clients.data.model.Client
import kotlinx.coroutines.delay
import java.util.*

class ClientRepositoryImpl : ClientRepository {

    private val clients = mutableMapOf<UUID, Client>()

    override suspend fun addClient(weight: Int, dateOfBirth: Date, photo: Uri) {
        delay(NETWORK_LATENCY_IN_MILLIS)
        val id = UUID.randomUUID()
        clients[id] = Client(id, weight, dateOfBirth, photo)
    }

    override suspend fun updateClient(id: UUID, weight: Int, dateOfBirth: Date, photo: Uri) {
        delay(NETWORK_LATENCY_IN_MILLIS)
        if (clients[id] != null) {
            clients[id] = Client(id, weight, dateOfBirth, photo)
        }
    }

    override suspend fun removeClient(id: UUID): Client? {
        delay(NETWORK_LATENCY_IN_MILLIS)
        return clients.remove(id)
    }

    override suspend fun getClient(id: UUID): Client? {
        delay(NETWORK_LATENCY_IN_MILLIS)
        return clients[id]
    }

    override suspend fun getClients(): List<Client> {
        delay(NETWORK_LATENCY_IN_MILLIS)
        return clients.values.toList()
    }

    companion object {

        private const val NETWORK_LATENCY_IN_MILLIS = 1000L
    }
}
