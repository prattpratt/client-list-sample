package com.example.clients.data.model

import android.net.Uri
import java.util.*

data class Client(
    val id: UUID,
    val weight: Int,
    val dateOfBirth: Date,
    val photo: Uri,
)
