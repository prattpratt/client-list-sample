package com.example.clients.list

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.clients.R
import com.example.clients.data.model.Client
import com.example.clients.ktx.DateStyle
import com.example.clients.ktx.inflate
import com.example.clients.ktx.toString
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_client.*

class ClientListAdapter(
    private val listener: Listener
) : ListAdapter<Client, ClientListAdapter.ViewHolder>(Companion) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(parent.inflate(R.layout.item_client))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position), position)

    inner class ViewHolder(
        override val containerView: View,
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        private val context = containerView.context

        fun bind(client: Client, position: Int) {
            Glide.with(photo)
                .load(client.photo)
                .centerCrop()
                .placeholder(R.drawable.ic_image_50)
                .error(R.drawable.ic_broken_image_50)
                .into(photo)
            weight.text = context.getString(R.string.weight_kg, client.weight)
            dateOfBirth.text = client.dateOfBirth.toString(DateStyle.MEDIUM)
            edit.setOnClickListener {
                listener.onEditClient(client, position)
            }
            delete.setOnClickListener {
                listener.onDeleteClient(client, position)
            }
            photo.setOnClickListener {
                listener.onClickPhoto(client, position)
            }
        }
    }

    interface Listener {

        fun onEditClient(client: Client, position: Int)

        fun onDeleteClient(client: Client, position: Int)

        fun onClickPhoto(client: Client, position: Int)
    }

    companion object : DiffUtil.ItemCallback<Client>() {

        override fun areItemsTheSame(
            oldItem: Client,
            newItem: Client
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: Client,
            newItem: Client
        ): Boolean = oldItem == newItem
    }
}
