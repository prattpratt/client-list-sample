package com.example.clients.list

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.clients.R
import com.example.clients.data.model.Client
import com.example.clients.edit.EditClientActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mikepenz.aboutlibraries.LibsBuilder
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_client_list.*
import kotlinx.android.synthetic.main.content_client_list.*

@AndroidEntryPoint
class ClientListActivity : AppCompatActivity(), ClientListAdapter.Listener {

    private val model by viewModels<ClientListViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_client_list)
        setSupportActionBar(toolbar)
        swipeRefresh.setOnRefreshListener {
            model.loadClients()
        }
        val clientAdapter = ClientListAdapter(this)
        clientList.run {
            adapter = clientAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
        addClient.setOnClickListener {
            EditClientActivity.startForResult(this, null)
        }
        model.progress.observe(this) {
            swipeRefresh.isRefreshing = it
        }
        model.clients.observe(this) {
            clientAdapter.submitList(it)
            clientList.isGone = it.isEmpty()
            noClients.isGone = it.isNotEmpty()
        }
        model.loadClients()
    }

    override fun onEditClient(client: Client, position: Int) {
        EditClientActivity.startForResult(this, client.id)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (EditClientActivity.isClientEdited(requestCode, resultCode)) {
            model.loadClients()
        }
    }

    override fun onDeleteClient(client: Client, position: Int) {
        MaterialAlertDialogBuilder(this)
            .setTitle(R.string.title_delete)
            .setMessage(R.string.message_delete)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                model.deleteClient(client.id)
            }.setNegativeButton(android.R.string.cancel) { dialog, _ ->
                dialog.cancel()
            }.show()
    }

    override fun onClickPhoto(client: Client, position: Int) {
        val intent = Intent().apply {
            action = Intent.ACTION_VIEW
            data = client.photo
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        }
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.about, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.actionAbout -> {
                LibsBuilder()
                    .withLicenseShown(true)
                    .withVersionShown(true)
                    .withEdgeToEdge(true)
                    .withActivityTitle(getString(R.string.about))
                    .withAboutIconShown(false)
                    .start(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
