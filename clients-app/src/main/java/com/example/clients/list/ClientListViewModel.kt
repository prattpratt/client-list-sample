package com.example.clients.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.clients.BaseViewModel
import com.example.clients.data.ClientRepository
import com.example.clients.data.model.Client
import com.example.clients.ktx.remove
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class ClientListViewModel
@Inject constructor(
    private val clientRepository: ClientRepository
) : BaseViewModel() {

    private val _clients = MutableLiveData<List<Client>>()
    val clients: LiveData<List<Client>>
        get() = _clients

    fun loadClients() {
        viewModelScope.launch {
            _progress.postValue(true)
            _clients.postValue(clientRepository.getClients())
            _progress.postValue(false)
        }
    }

    fun deleteClient(id: UUID) {
        viewModelScope.launch {
            _progress.postValue(true)
            _clients.remove(clientRepository.removeClient(id))
            _progress.postValue(false)
        }
    }
}
