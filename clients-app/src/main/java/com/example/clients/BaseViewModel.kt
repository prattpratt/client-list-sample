package com.example.clients

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel() {

    protected val _progress = MutableLiveData(false)
    val progress: LiveData<Boolean>
        get() = _progress
}
