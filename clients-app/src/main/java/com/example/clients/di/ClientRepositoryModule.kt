package com.example.clients.di

import com.example.clients.data.ClientRepository
import com.example.clients.data.ClientRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ClientRepositoryModule {

    @Provides
    @Singleton
    fun clientRepository(): ClientRepository = ClientRepositoryImpl()
}
