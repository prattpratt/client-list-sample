package com.example.clients.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.clients.R
import com.example.clients.ktx.DateStyle
import com.example.clients.ktx.toString
import com.google.android.material.datepicker.MaterialDatePicker
import kotlinx.android.synthetic.main.fragment_date_of_birth.*
import java.util.*

class DateOfBirthFragment : Fragment() {

    private val model by activityViewModels<EditClientViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_date_of_birth, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dateOfBirth.setOnClickListener {
            val date = model.dateOfBirth.value ?: Date()
            MaterialDatePicker.Builder.datePicker()
                .setSelection(date.time)
                .build().apply {
                    addOnPositiveButtonClickListener {
                        model.setDateOfBirth(Date(it))
                    }
                }.show(childFragmentManager, null)
        }
        model.progress.observe(viewLifecycleOwner) {
            dateOfBirth.isEnabled = !it
        }
        model.dateOfBirth.observe(viewLifecycleOwner) {
            dateOfBirth.setText(it.toString(DateStyle.MEDIUM))
        }
    }

    companion object {

        fun newInstance() = DateOfBirthFragment()
    }
}
