package com.example.clients.edit

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.clients.BuildConfig
import com.example.clients.R
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_edit_client.*
import kotlinx.android.synthetic.main.content_edit_client.*
import java.util.*

@AndroidEntryPoint
class EditClientActivity : AppCompatActivity() {

    private val model by viewModels<EditClientViewModel>()
    private val clientId by lazy { intent.getSerializableExtra(EXTRA_CLIENT_ID) as? UUID }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_client)
        swipeRefresh.setOnRefreshListener {
            model.loadClient(clientId)
        }
        val editClientAdapter = EditClientAdapter(this)
        viewPager.adapter = editClientAdapter
        viewPager.isUserInputEnabled = false
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {

            override fun onPageSelected(position: Int) {
                toolbar.setTitle(EditPage.values()[position].titleResId)
                next.setText(if (position == editClientAdapter.itemCount - 1) R.string.done else R.string.next)
            }
        })
        next.setOnClickListener {
            if (pageIndicator.selectedTabPosition == editClientAdapter.itemCount - 1) {
                model.completeEdit {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            } else {
                viewPager.setCurrentItem(viewPager.currentItem + 1, true)
            }
        }
        back.setOnClickListener { onBackPressed() }
        TabLayoutMediator(pageIndicator, viewPager) { tab, _ ->
            tab.view.isClickable = false
        }.attach()
        model.progress.observe(this) {
            swipeRefresh.isRefreshing = it
        }
        model.loadClient(clientId)
    }

    override fun onBackPressed() {
        if (viewPager.currentItem == 0) {
            super.onBackPressed()
        } else {
            viewPager.setCurrentItem(viewPager.currentItem - 1, true)
        }
    }

    companion object {

        private const val EXTRA_CLIENT_ID = "${BuildConfig.APPLICATION_ID}.extra.CLIENT_ID"
        private const val RC_CLIENT_EDITED = 101

        fun isClientEdited(requestCode: Int, resultCode: Int): Boolean {
            return requestCode == RC_CLIENT_EDITED && resultCode == RESULT_OK
        }

        fun startForResult(activity: Activity, clientId: UUID?) {
            activity.startActivityForResult(Intent(activity, EditClientActivity::class.java).apply {
                putExtra(EXTRA_CLIENT_ID, clientId)
            }, RC_CLIENT_EDITED)
        }
    }
}
