package com.example.clients.edit

import androidx.annotation.StringRes
import com.example.clients.R

enum class EditPage(@StringRes val titleResId: Int) {

    BODY_WEIGHT(R.string.enter_body_weight),

    DATE_OF_BIRTH(R.string.enter_date_of_birth),

    PHOTO(R.string.photo),
}
