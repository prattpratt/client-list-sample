package com.example.clients.edit

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.clients.BaseViewModel
import com.example.clients.data.ClientRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CompletionHandler
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class EditClientViewModel
@Inject constructor(
    private val clientRepository: ClientRepository
) : BaseViewModel() {

    private var _id: UUID? = null

    private val _weight = MutableLiveData<Int>()
    val weight: LiveData<Int>
        get() = _weight

    private val _dateOfBirth = MutableLiveData<Date>()
    val dateOfBirth: LiveData<Date>
        get() = _dateOfBirth

    private val _photo = MutableLiveData<Uri>()
    val photo: LiveData<Uri>
        get() = _photo

    fun loadClient(id: UUID?) {
        if (id == null) {
            _progress.postValue(false)
            return
        }
        viewModelScope.launch {
            _progress.postValue(true)
            clientRepository.getClient(id)?.let {
                _id = it.id
                _weight.postValue(it.weight)
                _dateOfBirth.postValue(it.dateOfBirth)
                _photo.postValue(it.photo)
            }
            _progress.postValue(false)
        }
    }

    fun setDateOfBirth(date: Date) {
        _dateOfBirth.postValue(date)
    }

    fun setWeight(value: Int) {
        _weight.postValue(value)
    }

    fun setPhoto(value: Uri) {
        _photo.postValue(value)
    }

    fun completeEdit(onComplete: CompletionHandler) {
        viewModelScope.launch {
            _progress.postValue(true)
            val weight = _weight.value
            val dateOfBirth = _dateOfBirth.value
            val photo = _photo.value
            if (weight == null || dateOfBirth == null || photo == null) {
                return@launch
            }
            val id = _id
            if (id == null) {
                clientRepository.addClient(weight, dateOfBirth, photo)
            } else {
                clientRepository.updateClient(id, weight, dateOfBirth, photo)
            }
            _progress.postValue(false)

        }.invokeOnCompletion(onComplete)
    }
}
