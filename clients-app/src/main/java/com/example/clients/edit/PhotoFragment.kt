package com.example.clients.edit

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.clients.R
import kotlinx.android.synthetic.main.fragment_photo.*

class PhotoFragment : Fragment() {

    private val model by activityViewModels<EditClientViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_photo, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        photoUri.setOnClickListener {
            val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "image/*"
            }
            startActivityForResult(intent, RC_CHOOSE_IMAGE)
        }
        model.progress.observe(viewLifecycleOwner) {
            photoUri.isEnabled = !it
        }
        model.photo.observe(viewLifecycleOwner) {
            photoUri.setText(it?.toString())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == RC_CHOOSE_IMAGE) {
            data?.data?.let {
                // The following line fixes issue with image loading on Android 11
                // SecurityException: Permission Denial:
                // opening provider com.android.providers.media.MediaDocumentsProvider
                // from ProcessRecord{d89d1f3 4644:com.example.clients.android/u0a121} (pid=4644, uid=10121)
                // requires that you obtain access using ACTION_OPEN_DOCUMENT or related APIs
                context?.contentResolver?.takePersistableUriPermission(
                    it,
                    Intent.FLAG_GRANT_READ_URI_PERMISSION
                )
                model.setPhoto(it)
            }
        }
    }

    companion object {
        private const val RC_CHOOSE_IMAGE = 101

        fun newInstance() = PhotoFragment()
    }
}
