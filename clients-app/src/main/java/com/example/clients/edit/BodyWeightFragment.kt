package com.example.clients.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.example.clients.R
import kotlinx.android.synthetic.main.fragment_body_weight.*

class BodyWeightFragment : Fragment() {

    private val model by activityViewModels<EditClientViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_body_weight, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model.progress.observe(viewLifecycleOwner) {
            bodyWeight.isEnabled = !it
        }
        model.weight.observe(viewLifecycleOwner) {
            it.toString().run {
                if (this != bodyWeight.text?.toString()) {
                    bodyWeight.setText(this)
                }
            }
        }
        bodyWeight.addTextChangedListener {
            it?.toString()?.toIntOrNull()?.let { model.setWeight(it) }
        }
    }

    companion object {

        fun newInstance() = BodyWeightFragment()
    }
}
