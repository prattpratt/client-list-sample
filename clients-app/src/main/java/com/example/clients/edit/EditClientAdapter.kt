package com.example.clients.edit

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class EditClientAdapter(
    activity: EditClientActivity,
) : FragmentStateAdapter(activity) {

    override fun getItemCount(): Int {
        return EditPage.values().size
    }

    override fun createFragment(position: Int): Fragment {
        return when (EditPage.values()[position]) {
            EditPage.BODY_WEIGHT -> BodyWeightFragment.newInstance()
            EditPage.DATE_OF_BIRTH -> DateOfBirthFragment.newInstance()
            EditPage.PHOTO -> PhotoFragment.newInstance()
        }
    }
}
